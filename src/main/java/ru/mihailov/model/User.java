package ru.mihailov.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@Entity
@Table(name = "app_User")
public class User {

   @Id
   private String id = UUID.randomUUID().toString();

   private String name;

   private String email;

   private String login;

   private String passwordHash;

   @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
   private List<Role> roles = new ArrayList<>();

   @Override
   public String toString() {
      return "User{" +
              "id='" + id + '\'' +
              ", name='" + name + '\'' +
              ", email='" + email + '\'' +
              ", login='" + login + '\'' +
              ", passwordHash='" + passwordHash + '\'' +
              '}';
   }

}
