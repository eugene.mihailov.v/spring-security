package ru.mihailov.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.mihailov.model.Role;
import ru.mihailov.model.User;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findByLogin(username);
        if (user == null) {
            throw new UsernameNotFoundException("User has not been found!");
        }
        org.springframework.security.core.userdetails.User.UserBuilder userBuilder = null;
        userBuilder = org.springframework.security.core.userdetails.User.withUsername(username);
        userBuilder.password(user.getPasswordHash());
        final List<Role> userRoles = user.getRoles();
        final List<String> roles = new ArrayList<>();
        userRoles.forEach(role -> roles.add(role.getRoleType().toString()));
        userBuilder.roles(roles.toArray(new String[] {}));
        return userBuilder.build();
    }

}
