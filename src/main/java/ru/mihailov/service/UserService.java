package ru.mihailov.service;

import java.util.List;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.mihailov.model.Role;
import ru.mihailov.repository.UserRepository;
import ru.mihailov.model.User;
import ru.mihailov.type.RoleType;

@Service
public class UserService {

   @Autowired
   private UserRepository userRepository;

   @Autowired
   private RoleService roleService;

   @Autowired
   private BCryptPasswordEncoder passwordEncoder;

   @Transactional
   public User save(User user) {
      return userRepository.save(user);
   }

   @Transactional(readOnly = true)
   public List<User> list() {
      return userRepository.findAll();
   }

   @Transactional(readOnly = true)
   public User findByLogin(String username) {
      return userRepository.findByLogin(username);
   }

   @PostConstruct
   private void postConstruct() {
      User user = new User();
      user.setName("Bill Gates");
      user.setEmail("1st@microsoft.com");
      user.setLogin("admin");
      user.setPasswordHash(passwordEncoder.encode("123456"));

      user = save(user);

      Role role  = new Role();
      role.setRoleType(RoleType.ADMINISTRATOR);
      role.setUser(user);
      roleService.save(role);
   }

}
